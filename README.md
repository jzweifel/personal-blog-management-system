# Personal Blog Management System #


### What is this repository for? ###

This is currently a fairly basic blog with the intention of evolving into something a bit more advanced. It is based off of a simple blog created in the Comprehensive ASP.NET MVC course on Udemy.

Current objectives:

* Implement user registration form
* Implement blog post comment system
* Facebook/Twitter hooks? (New post created -> post to social media)

### Technologies Used ###

* ASP.NET MVC
* nHibernate
* Fluent Migrator
* jQuery
* Twitter Bootstrap
* MySQL Community Edition
* ELMAH

### Who do I talk to? ###

* [Jacob Zweifel](mailto:jacob@jacobzweifel.com)