﻿using System;
using System.Collections.Generic;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;


namespace PersonalBlogManagementSystem.Models
{
    public class Post
    {
        public virtual int Id { get; set; }
        public virtual User User { get; set; }

        public virtual string Title { get; set; }
        public virtual string Slug { get; set; }
        public virtual string Content { get; set; }

        public virtual DateTime Created { get; set; }
        public virtual DateTime? Updated { get; set; }
        public virtual DateTime? Deleted { get; set; }

        public virtual IList<Tag> Tags { get; set; }

        public virtual bool IsDeleted { get { return Deleted != null; } }

        public Post()
        {
            Tags = new List<Tag>();
        }
    }

    public class PostMap : ClassMapping<Post>
    {
        public PostMap()
        {
            Table("posts");

            Id(x => x.Id, x => x.Generator(Generators.Identity));

            ManyToOne(x => x.User, x =>
                {
                    x.Column("userid");
                    x.NotNullable(true);
                });

            Property(x => x.Title, x => x.NotNullable(true));
            Property(x => x.Slug, x => x.NotNullable(true));
            Property(x => x.Content, x => x.NotNullable(true));

            Property(x => x.Created, x => x.NotNullable(true));
            Property(x => x.Updated, x => x.NotNullable(false));
            Property(x => x.Deleted, x => x.NotNullable(false));

            Bag(x => x.Tags, x =>
            {
                x.Key(y => y.Column("postid"));
                x.Table("post_tags");
            }, x => x.ManyToMany(y => y.Column("tagid")));
        }
    }
}