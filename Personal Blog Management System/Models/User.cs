﻿using System.Collections.Generic;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Linq;
using System.Linq;

namespace PersonalBlogManagementSystem.Models
{
    public class User
    {
        private const int WorkFactor = 13;

        public virtual int Id { get; set; }
        public virtual string Username { get; set; }
        public virtual string Email { get; set; }
        public virtual string PasswordHash { get; set; }

        public virtual IList<Role> Roles { get; set; }

        public User()
        {
            Roles = new List<Role>();
        }

        public virtual void SetPassword(string password)
        {
            PasswordHash = BCrypt.Net.BCrypt.HashPassword(password, WorkFactor);
        }

        public virtual bool CheckPassword(string password)
        {
            return BCrypt.Net.BCrypt.Verify(password, PasswordHash);
        }

        public static void FakeHash()
        {
            BCrypt.Net.BCrypt.HashPassword("", WorkFactor);
        }

        //public static void CreateAdminPassword()
        //{
        //    var adminusr = Database.Session.Query<User>().FirstOrDefault(u => u.Username == "admin");

        //    adminusr.SetPassword("test");

        //    Database.Session.Update(adminusr);
        //}
    }

    public class UserMap : ClassMapping<User>
    {
        public UserMap()
        {
            Table("users");
            Id(x => x.Id, x => x.Generator(Generators.Identity));
            Property(x => x.Username, x => x.NotNullable(true));
            Property(x => x.Email, x => x.NotNullable(false));
            Property(x => x.PasswordHash, x => 
                        { 
                            x.Column("password_hash");
                            x.NotNullable(true);
                        });

            Bag(x => x.Roles, x =>
                {
                    x.Table("role_users");
                    x.Key(k => k.Column("userid"));
                }, x => x.ManyToMany(k => k.Column("roleid")));
        }
    }
}