﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using NHibernate.Linq;
using PersonalBlogManagementSystem.Models;
using PersonalBlogManagementSystem.ViewModels;

namespace PersonalBlogManagementSystem.Controllers
{
    public class AuthController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Login(AuthLogin form, string returnUrl)
        {
            if (User == null)
                PersonalBlogManagementSystem.Models.User.FakeHash();

            var user = Database.Session.Query<User>().FirstOrDefault(u => u.Username == form.Username);
            if (user == null || !user.CheckPassword(form.Password))
                ModelState.AddModelError("Username", "Username or password is incorrect");

            if (!ModelState.IsValid)
                return View(form);

            FormsAuthentication.SetAuthCookie(user.Username, true);

            if (!string.IsNullOrWhiteSpace(returnUrl))
                return Redirect(returnUrl);

            return RedirectToRoute("home");
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToRoute("home");
        }
	}
}