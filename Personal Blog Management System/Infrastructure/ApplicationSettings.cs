﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace PersonalBlogManagementSystem.Infrastructure
{
    public class ApplicationSetting
    {
        public virtual int Id { get; set; }
        public virtual string Setting { get; set; }
        public virtual string Value { get; set; }

        public static List<ApplicationSetting> GetAllSettings()
        {
            //Since this could be called from an actual request OR
            //at app start, we should open a session
            //TODO: Analyse actual impact of this code
            if (Database.Session == null)
                Database.OpenSession();

            var settings = Database.Session.Query<ApplicationSetting>()
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .ToList();

            return settings;
        }

        public static string GetSetting(string name)
        {
            List<ApplicationSetting> settings = GetAllSettings();

            if (settings == null)
                return null;

            ApplicationSetting setting = settings.Find(s => s.Setting == name);

            if (setting == null || setting.Value == "")
                return null;

            return setting.Value;
        }

        public static void AddSetting(string name, string value)
        {
            var newSetting = new ApplicationSetting
            {
                Setting = name,
                Value = value
            };

            if (Database.Session.Query<ApplicationSetting>().Any(a => a.Setting == name))
                return;

            Database.Session.SaveOrUpdate(newSetting);

            GetAllSettings();          
        }

        public static void SetSetting(string name, string value)
        {
            var newSetting = Database.Session.Query<ApplicationSetting>().FirstOrDefault(a => a.Setting == name);

            if (newSetting == null)
                return;

            newSetting.Value = value;

            Database.Session.SaveOrUpdate(newSetting);

            GetAllSettings();
        }
    }

    public class ApplicationSettingsMap : ClassMapping<ApplicationSetting>
    {
        public ApplicationSettingsMap()
        {
            Table("application_settings");

            Id(x => x.Id, x => x.Generator(Generators.Identity));
            Property(x => x.Setting, x => x.NotNullable(true));
            Property(x => x.Value, x => x.NotNullable(true));
        }
    }
}