﻿using System.Web.Mvc;

namespace PersonalBlogManagementSystem.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName { get { return "admin"; } }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AdminHome",
                "admin/",
                new { controller = "admin", action = "Index" });

            context.MapRoute(
                "Admin_default",
                "admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            //Layout routes
            context.MapRoute("AdminSidebar", "", new { controller = "Layout", action = "Sidebar" });
        }
    }
}