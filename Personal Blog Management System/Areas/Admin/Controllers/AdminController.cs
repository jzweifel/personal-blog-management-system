﻿using System.Web.Mvc;
using PersonalBlogManagementSystem.Infrastructure;

namespace PersonalBlogManagementSystem.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/Admin/
        [SelectedTabAttribute("adminhome")]
        public ActionResult Index()
        {
            return View();
        }
	}
}