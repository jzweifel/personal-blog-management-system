﻿using System.Web.Mvc;

namespace PersonalBlogManagementSystem.Areas.Admin.Controllers
{
    public class LayoutController : Controller
    {
        [ChildActionOnly]
        public ActionResult Sidebar()
        {
            return View();
        }
	}
}