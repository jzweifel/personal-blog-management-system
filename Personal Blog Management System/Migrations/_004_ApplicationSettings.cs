﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentMigrator;

namespace PersonalBlogManagementSystem.Migrations
{
    [Migration(4)]
    public class _004_ApplicationSettings : Migration
    {
        public override void Up()
        {
            Create.Table("application_settings")
                .WithColumn("id").AsInt32().PrimaryKey().Identity().NotNullable()
                .WithColumn("setting").AsString(128).Unique().NotNullable()
                .WithColumn("value").AsString(128).NotNullable();
        }

        public override void Down()
        {
            Delete.Table("application_settings");
        }
    }
}