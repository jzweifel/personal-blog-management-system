﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using FluentMigrator;

namespace PersonalBlogManagementSystem.Migrations
{
    [Migration(1)]
    public class _001_UsersAndRoles : Migration
    {
        public override void Up()
        {
            Create.Table("users")
                  .WithColumn("id").AsInt32().Identity().PrimaryKey().NotNullable()
                  .WithColumn("username").AsString(128).NotNullable()
                  .WithColumn("email").AsCustom("VARCHAR(256)").Nullable()
                  .WithColumn("password_hash").AsString(128).NotNullable();

            Create.Table("roles")
                  .WithColumn("id").AsInt32().Identity().PrimaryKey().NotNullable()
                  .WithColumn("name").AsString(128).NotNullable();


            Create.Table("role_users")
                  .WithColumn("id").AsInt32().Identity().PrimaryKey().NotNullable()
                  .WithColumn("userid").AsInt32().ForeignKey("users", "id").OnDelete(Rule.Cascade).NotNullable()
                  .WithColumn("roleid").AsInt32().ForeignKey("roles", "id").OnDelete(Rule.Cascade).NotNullable();
        }

        public override void Down()
        {
            Delete.Table("role_users");
            Delete.Table("roles");
            Delete.Table("users");
        }
    }
}