﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentMigrator;

namespace PersonalBlogManagementSystem.Migrations
{
    [Migration(5)]
    public class _005_ForceUsernameColumnUnique : Migration
    {
        public override void Up()
        {
            Alter.Column("Username").OnTable("users").AsString(128).Unique().NotNullable();
        }

        public override void Down()
        {
            Alter.Column("username").OnTable("users").AsString(128).NotNullable();
        }
    }
}