﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using FluentMigrator;

namespace PersonalBlogManagementSystem.Migrations
{
    [Migration(2)]
    public class _002_PostsAndTags : Migration
    {
        public override void Up()
        {
            Create.Table("posts")
                  .WithColumn("id").AsInt32().PrimaryKey().Identity().NotNullable()
                  .WithColumn("userid").AsInt32().ForeignKey("users", "id").NotNullable()
                  .WithColumn("title").AsString(128).NotNullable()
                  .WithColumn("slug").AsString(128).NotNullable()
                  .WithColumn("created").AsDateTime().NotNullable()
                  .WithColumn("updated").AsDateTime().Nullable()
                  .WithColumn("deleted").AsDateTime().Nullable();


            Create.Table("tags")
                  .WithColumn("id").AsInt32().PrimaryKey().Identity().NotNullable()
                  .WithColumn("slug").AsString(128)
                  .WithColumn("name").AsString(128);

            Create.Table("post_tags")
                  .WithColumn("id").AsInt32().PrimaryKey().Identity().NotNullable()
                  .WithColumn("tagid").AsInt32().ForeignKey("tags", "id").OnDelete(Rule.Cascade)
                  .WithColumn("postid").AsInt32().ForeignKey("posts", "id").OnDelete(Rule.Cascade);
        }

        public override void Down()
        {
            Delete.Table("post_tags");
            Delete.Table("tags");
            Delete.Table("posts");
        }
    }
}