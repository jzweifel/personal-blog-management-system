﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PersonalBlogManagementSystem.Controllers;

namespace PersonalBlogManagementSystem
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var namespaces = new[] {typeof (PostsController).Namespace};

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Home/site index route
            //blog.dev/
            routes.MapRoute("Home", "", new { controller = "Posts", action = "Index" }, namespaces);


            //Layout routes
            routes.MapRoute("Sidebar", "", new { controller = "Layout", action = "Sidebar" }, namespaces);


            //Authentication routes
            //blog.dev/login
            routes.MapRoute("Login", "login", new { controller = "Auth", action = "Login" }, namespaces);
            //blog.dev/login
            routes.MapRoute("Logout", "logout", new { controller = "Auth", action = "Logout" }, namespaces);

            
            //Post routes
            //blog.dev/post/1-this-is-an-example
            //used so routing engine knows what the hell is actually going on.
            //NOTE! Must be mapped first
            routes.MapRoute("PostRouting", "post/{idAndSlug}", new { controller = "Posts", action = "Show" }, namespaces);
            //used for URL generation
            routes.MapRoute("Post", "post/{id}-{slug}", new { controller = "Posts", action = "Show" }, namespaces);


            //Tag routes
            //blog.dev/tag/2-example-tag
            //used so routing engine knows what the hell is actually going on.
            //NOTE! Must be mapped first
            routes.MapRoute("TagRouting", "tag/{idAndSlug}", new { controller = "Posts", action = "Tag" }, namespaces);
            //used for URL generation
            routes.MapRoute("Tag", "tag/{id}-{slug}", new { controller = "Posts", action = "Tag" }, namespaces);

            //Custom error routing
            routes.MapRoute("Error500", "error/500", new { controller = "Errors", action = "Error" }, namespaces);
            routes.MapRoute("Error404", "error/404", new { controller = "Errors", action = "NotFound" }, namespaces);
        }
    }
}