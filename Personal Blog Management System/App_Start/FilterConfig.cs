﻿using System.Web.Mvc;
using PersonalBlogManagementSystem.Infrastructure;

namespace PersonalBlogManagementSystem
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new TransactionFilter());
        }
    }
}