﻿using PersonalBlogManagementSystem.Infrastructure;
using NHibernate.Linq;
using System.Linq;
using PersonalBlogManagementSystem.Models;

namespace PersonalBlogManagementSystem.App_Start
{
    public class ApplicationSettings
    {
        public static void Initialize()
        {
            var isInitialized = Infrastructure.ApplicationSetting.GetSetting("isInitialized");

            if (isInitialized == null || isInitialized == "false")
            {
                using (var tx = Database.Session.BeginTransaction())
                {


                    //let's set up a user
                    var adminUser = new Models.User
                    {
                        Username = "admin",
                        Email = "admin@blog.dev"
                    };

                    adminUser.SetPassword("test");
                    Database.Session.Save(adminUser);

                    //now set up the admin role
                    var adminRole = new Models.Role
                    {
                        Name = "admin"
                    };

                    Database.Session.Save(adminRole);
                    tx.Commit();
                }


                //create the user/role relationship
                using (var tx = Database.Session.BeginTransaction())
                {
                    var newAdminUser = Database.Session.Query<User>().FirstOrDefault(u => u.Username == "admin");
                    var newAdminRole = Database.Session.Query<Role>().FirstOrDefault(r => r.Name == "admin");
                    Database.Session.CreateSQLQuery(string.Format(
                        "INSERT INTO role_users (roleid, userid) values ({0}, {1})",
                        newAdminRole.Id, newAdminUser.Id)).UniqueResult();
                    tx.Commit();
                }

                //that is really all we need to be functional
                //we should have an initialized DB
                using (var tx = Database.Session.BeginTransaction())
                {
                    if (isInitialized == null)
                        Infrastructure.ApplicationSetting.AddSetting("isInitialized", "true");
                    else
                        Infrastructure.ApplicationSetting.SetSetting("isInitialized", "true");

                    tx.Commit();
                }
            }
        }
    }
}