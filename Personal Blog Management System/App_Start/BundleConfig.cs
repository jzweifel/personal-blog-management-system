﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace PersonalBlogManagementSystem.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/admin/styles")
                .Include("~/content/styles/bootstrap.css")
                .Include("~/content/styles/admin.css")
                );

            bundles.Add(new StyleBundle("~/styles")
                .Include("~/content/styles/bootstrap.css")
                .Include("~/content/styles/site.css")
                );

            bundles.Add(new ScriptBundle("~/allscripts")
                .Include("~/scripts/jquery-2.1.1.js")
                .Include("~/scripts/jquery.validate.js")
                .Include("~/scripts/jquery.validate.unobtrusive.js")
                .Include("~/scripts/bootstrap.js")
                .Include("~/scripts/jquery.timeago.js")
                .Include("~/scripts/frontend.js")
                );

            bundles.Add(new ScriptBundle("~/adminscripts")
                .Include("~/areas/admin/scripts/Forms.js")
                );

            bundles.Add(new ScriptBundle("~/adminscripts/posts")
                .Include("~/areas/admin/scripts/PostEditor.js")
                );            
        }
    }
}